<?php

namespace Drupal\chemistry_defaults\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\ClearCacheForm;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide settings form for microsites.
 */
class MicrositeSettingsForm extends ConfigFormBase {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a MicrositeSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   Form builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FormBuilderInterface $form_builder) {
    $this->configFactory = $config_factory;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // This form is a slight hack, to provide microsite admins restricted
    // ability to set some site settings. We thus return an empty array here,
    // and manually update config objects for ourselves.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chemistry_defaults_microsite_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $site_config = $this->config('system.site');

    $form['site_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => $site_config->get('name'),
      '#required' => TRUE,
    ];

    // See core/modules/system/src/Controller/PerformanceController.php
    // We inject the form_builder service into this class to emulate
    // PerformanceController.
    $form['clear_cache'] = $this->formBuilder->getForm(ClearCacheForm::class);
    $form['clear_cache']['clear_cache']['helptext'] = [
      '#markup' => '<div>Users who are not logged in may see a slightly out-of-date version of some site content, because the content may have been cached on the server. You can clear all server-side caches by clicking the button above.<br />Some suggested reasons for doing this are to ensure that the site is functioning as expected when developing new content, or to ensure that highly time-sensitive content is immediately made visible.</div>',
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_config = $this->configFactory->getEditable('system.site');
    $site_config->set('name', $form_state->getValue('site_title'))->save();

    parent::submitForm($form, $form_state);
  }

}

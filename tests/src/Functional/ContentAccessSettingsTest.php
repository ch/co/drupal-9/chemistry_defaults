<?php

namespace Drupal\Tests\chemistry_defaults\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Check our settings for content_access.
 *
 * Test is not running reliably. I have suspicious about calling
 * content_access_get_settings() and how it loads config inside
 * a test env.
 *
 * @group skip
 */
class ContentAccessSettingsTest extends BrowserTestBase {

  /**
   * Modules that this test suite depends upon.
   *
   * @var array
   */
  public static $modules = ['chemistry_defaults'];

  /**
   * Theme to use for tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Data provider for testPerNodeSettings.
   *
   * @return array
   *   Expected per-node setting for given bundles
   */
  public function perNodeSettingsDataProvider() {
    yield 'per-node setting for page' => ['page', 1];
  }

  /**
   * Test the per_node content access setting.
   *
   * @dataProvider perNodeSettingsDataProvider()
   */
  public function testPerNodeSettings(string $bundle, int $expected_value) {
    $setting = content_access_get_settings('per_node', $bundle);
    $this->assertEquals($expected_value, $setting);
  }

}

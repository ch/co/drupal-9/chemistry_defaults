<?php

namespace Drupal\Tests\chemistry_defaults\Functional;

use Drupal\entityqueue\Entity\EntityQueue;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests of front page nodequeue functionality.
 *
 * @group chemistry_defaults
 */
class FrontPageEntityQueueTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['chemistry_defaults', 'block', 'update'];

  /**
   * Theme to use for tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Name of the front page queue configured by chemistry_defaults.
   *
   * Needs to match chemistry_defaults.install setting.
   */
  const FRONTPAGE_QUEUE = 'front_page_teasers';

  /**
   * Test front page queue has been set up.
   */
  public function testFrontPageQueueExists() {
    $queue = EntityQueue::load(FrontPageEntityQueueTest::FRONTPAGE_QUEUE);
    $this->assertEquals($queue->id(), FrontPageEntityQueueTest::FRONTPAGE_QUEUE);
  }

}
